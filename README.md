# Vuejs slides with revealjs

Visit page in page in https://danigayosog.gitlab.io/hack-a-boss-vue-slides

## Development 
To develop and se changes without refresh browser manually
```bash
cd public/revealjs/
npm install
npm run start
```

Add as many file slides as you want, and modify public/index.html to link new slides.

To update revealjs download the new version and override files inside public/revealjs (make a backup of slides files
vue.html, vue-router just in case)
